import { Component, Input, OnInit } from '@angular/core';
import { timer } from 'rxjs';
import { EmployeesService } from 'src/app/services/employees.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public restaurants:any=[]
  public reviews :any=[]
  review:boolean=false
  showAll:boolean=false;
  restaurantName=""
  isLoader:boolean=true
  constructor(private restaurantService:EmployeesService) { }
  
  async ngOnInit(): Promise<void> {

    this.restaurants= await this.restaurantService.getRestaurant()
    this.isLoader=false
    console.log("ew",this.isLoader)
  }
  
  addReview()
  {
    this.review=!this.review;
  }

  filter(val:any)
  {
      const result = timer(2000)
      alert('Here are your filtered results')
      this.isLoader=true
      result.subscribe(()=>
      {
        this.isLoader=false
        this.restaurants=this.restaurants.filter((elem:any)=>
      {
        if (val[0]==="rating")
          return elem.rating>=Number(val[1])

        else if (val[0]==="name")
          return elem.name===val[1]

        else
          return elem.id===val[1]
      })
      })
      
      
      
  }
  showReview(restName:string)
  {
    this.restaurantName=restName
    this.showAll=!this.showAll;

  }
  submitReview()
  {
    setTimeout(() => {
      this.review=false;
      alert("Your review has been submitted.Thank you for visiting our place")
      
    }, 1000);
    
  
  }

}
