import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css']
})
export class CounterComponent implements OnInit {

  hour=0
  min=0
  sec=0
  constructor() { }

  ngOnInit(): void {
    const broadcast =interval(1000)
    broadcast.subscribe((res)=>
    {
      
      this.min=this.min+ Math.floor((this.sec+1)/60)
      this.hour=this.hour+Math.floor(this.min/60)
      this.min%=60
      this.sec=(this.sec+1)%60
      
    })
    
  }


}
