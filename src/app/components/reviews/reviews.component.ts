import { Component, OnInit } from '@angular/core';
import { EmployeesService } from 'src/app/services/employees.service';
@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})
export class ReviewsComponent implements OnInit {

  public reviews:any=[]
  public reviewed:boolean=false;
  public reviewer =""
  public isAdmin = false
  public edit=false
  constructor(private review :EmployeesService) { }

  ngOnInit(): void {
    this.reviews=this.review.getReviews();
    let item:any=localStorage.getItem("user")
    item=JSON.parse(item)
    this.isAdmin=item.admin

  }
 async Edit(name:string)
  {
    
    
    
    
   
      let x:any= document.getElementById("comment")
      
      this.edit=!this.edit
      x.contentEditable=this.edit.toString()
      x.style.border=this.edit?"1px solid white" :""
      console.log(this.edit,x?.contentEditable)
  
    
  }

  Delete(name:string)
  {
      setTimeout(() => {
        alert(`${name} review is being deleted`)
        this.reviews=this.reviews.filter((elem: { name: string; })=>
        {
          return elem.name !=name
        })
      }, 500);
     
      
  }
  showReview(name:string)
  {
    this.reviewed=!this.reviewed
    this.reviewer=name
    this.edit=false
    
  }
  

}
