import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeesService } from 'src/app/services/employees.service';
import { FormGroup, FormControl} from '@angular/forms';
import { Validators} from '@angular/forms';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public x=false;
  constructor(private router:Router,private service:EmployeesService) { }
  formSubmitted = false;
  myForm!: FormGroup;
  title!: FormControl ;
  Name!: FormControl;
  Email!: FormControl;
  Password!: FormControl;
  brand!: FormControl;
  price!: FormControl;
  ngOnInit(): void {
  }

  register(email:string,password:string,admin:boolean,name:string)
  {
    let obj:any={"email":email,"password":password,"name":name}
    let y = this.service.Valid(obj)
    console.log(y)
    this.x = y==1?false:true
    if (y)
    {
      localStorage.setItem("user",JSON.stringify({"password":password,"email":email,"admin":admin,"loggedIn":false}))
      alert("Congratulations,You have been registered");
      this.router.navigate(["/login"])
    }
   
    
  }

}
