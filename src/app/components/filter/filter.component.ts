import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  @Output() Filter=new EventEmitter<any>()
  
  constructor() { }

  ngOnInit(): void {
  }

  filter(typeF:string,val:any)
  {
    this.Filter.emit([typeF,val])
  }

}
