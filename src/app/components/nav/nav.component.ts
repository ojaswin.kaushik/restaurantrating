import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router"
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  public isLoggedIn=false;
  constructor(private route:Router) { }

  ngOnInit(): void {
    let item:any=(localStorage.getItem("user"))
    if (item===null)
    {
      this.isLoggedIn=false;
      return
    }
    item=JSON.parse(item)
    this.isLoggedIn=item.loggedIn
  }
  logout()
  {
      let item:any=(localStorage.getItem("user"))
      item=JSON.parse(item)
      item.loggedIn=false
      this.isLoggedIn=item.loggedIn
      localStorage.setItem("user",JSON.stringify(item))
      alert("You have been successfully logged out")
      this.route.navigate(['login'])
      
  }

}
