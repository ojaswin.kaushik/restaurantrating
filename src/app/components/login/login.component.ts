import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeesService } from 'src/app/services/employees.service';
import validator from 'validator';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router:Router,private validator:EmployeesService) { }

  ngOnInit(): void {
  }

  Dashboard(email:string,password:string,admin:boolean)
  {
    
    if (!validator.isEmail(email) && !validator.isStrongPassword(password))
      {
        alert("enter correct login credentials ")
        return
      }

    
    let item:any =localStorage.getItem("user")
    
    if (item===null)
      {
        alert("Invalid Email , Please register to create a account")
        return 
      }
      item=JSON.parse(item)
      console.log(item)
    if(item.password!==password)
      {
        alert("invalid password , Please register to create a account ")
        return 
      }
      if (item.admin!==admin)
      {
        alert("Check the correct role(User/Admin)")
        return
      }
        
      if(!item.loggedIn)
        {
          item.loggedIn="true"
          localStorage.setItem("user",JSON.stringify(item))
          this.router.navigate(['/dashboard'])
          return
        }

      alert("You are already logged in!")
      this.router.navigate(['/dashboard'])

      
  }

}
