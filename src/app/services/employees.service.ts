import { Injectable } from '@angular/core';
import validator from 'validator';

@Injectable({
  providedIn: 'root'
}) 
export class EmployeesService {

  constructor() { 
  }
  Valid(obj:any)
  
  {
    //let flag=0
    var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");

    if (validator.isEmpty(obj.name))
    {
      alert("Name field should not be empty")
      return 0
    }
    if (!validator.isEmail(obj.email))
      {
        alert("enter correct email")
        return 0
    }
      
    if (!strongRegex.test(obj.password))
    {
      alert ("Not a strong password")
      return 0

    }
    

     
      
    

    return 1


    
    
    

  }
  sleep(ms:number)
  {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  getReviews()
  {
    return [
      {
        "name":"Max","comments":"“When you go out and hit the sauce. It’s gotta be HEINZ.”" ,
        "rating":3
      },
      {
        "name":"Joe","comments":"“When you go out and hit the sauce. It’s gotta be HEINZ.”" ,
        "rating":4
      },
      {
        "name":"Grey","comments":"“When you go out and hit the sauce. It’s gotta be HEINZ.”" ,
        "rating":1
      },
      {
        "name":"Anthony","comments":"“When you go out and hit the sauce. It’s gotta be HEINZ.”" ,
        "rating":5
      },
    ]
  }
  async getEmployees() 
  {
    console.log("Before wait")
    await this.sleep(3000);
    console.log("after wait")
    return [
      {'id':1,"img":"https://images.adsttc.com/media/images/5612/d666/e58e/ce03/3400/0012/large_jpg/Parq_(United_States).jpg?1444075081","name":"Rest1","description":"There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text,","rating":1,"location":"East","Contact":"123456"},
      {'id':1,"img":"https://images.adsttc.com/media/images/5612/d666/e58e/ce03/3400/0012/large_jpg/Parq_(United_States).jpg?1444075081","name":"Rest2","description":"There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text,","rating":2,"location":"East","Contact":"123456"},
      {'id':1,"img":"https://images.adsttc.com/media/images/5612/d666/e58e/ce03/3400/0012/large_jpg/Parq_(United_States).jpg?1444075081","name":"Rest3","description":"There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text,","rating":3,"location":"East","Contact":"123456"},
      {'id':1,"img":"https://images.adsttc.com/media/images/5612/d666/e58e/ce03/3400/0012/large_jpg/Parq_(United_States).jpg?1444075081","name":"Rest4","description":"There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text,","rating":4,"location":"East","Contact":"123456"},
    ]
  }
}
